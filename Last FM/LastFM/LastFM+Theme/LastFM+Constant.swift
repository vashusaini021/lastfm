//
//  LastFM+Constant.swift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

typealias LabelText = LastFM.LabelText
typealias Placeholder = LastFM.Placeholder

enum LastFM {
    
    struct Placeholder {
        
        static let search = "Search by song, artist, album"
    }
    
    struct LabelText {
        
        static let artists = "Artists"
        static let albums = "Albums"
        static let songs = "Songs"
    }
    
    struct NavigationTitle {
        
        static let lastFM = "Last FM"
    }
    
    struct AlertMessage {
        
        static let noInternet = "No Internet connection available. Please connect to internet and try again."
        static let somethingWentWrong = "Something went wrong!"
    }
    
    struct ApiKey {
        
        static let method = "method"
        static let apiKey = "api_key"
        static let format = "format"
        static let limit = "limit"
        static let page = "page"
        static let album = "album"
        static let artist = "artist"
        static let track = "track"
    }
}
