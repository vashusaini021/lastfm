//
//  UIViewController+Alert.swift
//  Last FM
//
//  Created by Vashu on 18/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func alert(message: String, title: String = "", oKAction: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: oKAction)
        
        alertController.addAction(OKAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
