//
//  HomeTableViewCell.swift
//  Last FM
//
//  Created by Vasu Saini on 14/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}
