//
//  HomeViewController.swift
//  Last FM
//
//  Created by Vasu Saini on 14/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import UIKit

typealias ApiKey = LastFM.ApiKey

enum ControllerType: String {
    case songs = "Songs"
    case artists = "Artists"
    case albums = "Albums"
    case lastFM = "Last FM"
}

class HomeViewController: UIViewController {
    
    
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: HomeTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HomeTableViewCell.self))
        }
    }
    
    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = LastFM.Placeholder.search
        searchController.searchBar.delegate = self
        searchController.searchBar.barStyle = .black
        searchController.searchBar.tintColor = .blue
        searchController.searchBar.barTintColor = .blue
        definesPresentationContext = true
        
        return searchController
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.tintColor = .black
        control.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        return control
    }()
    
    var categories = [LabelText.songs, LabelText.artists, LabelText.albums]
    
    var artists = [Artist]()
    var albums = [Album]()
    var tracks = [Track]()
    var controllerType = ControllerType.lastFM
    var searchedText = ""
    var pageNumber = 1
    var maxAlbumPageNumber = 1, maxTrackPageNumber = 1, maxArtistPageNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchBar.becomeFirstResponder()
        
        if categories.count == 1 {
            tableView.addSubview(refreshControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = controllerType.rawValue
        navigationItem.searchController = categories.count > 1 ? searchController : nil
    }
    
    @objc private func refreshData(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        pageNumber = 1
        switch controllerType {
        case .songs:
            getTracks(with: searchedText)
        case .artists:
            getArtists(with: searchedText)
        case .albums:
            getAlbums(with: searchedText)
        default:
            break
        }
    }
    
    private func getAlbums(with seachText: String) {
        LastFMNetworkManager.getServerData(withParamenters: [ApiKey.method : "album.search", ApiKey.album: seachText, ApiKey.limit: "20", ApiKey.page: "\(pageNumber)"], success: { results in
            guard let results = results, let albums = results.result?.albummatches?["album"] else { return }
            
            if self.pageNumber == 1 {
                self.albums = albums
            } else {
                self.albums.append(contentsOf: albums)
            }
            self.maxAlbumPageNumber = Int(results.result?.totalResults ?? "1") ?? 1
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { error in
            DispatchQueue.main.async {
                self.alert(message: error)
            }
        }
    }
    
    private func getArtists(with seachText: String) {
        LastFMNetworkManager.getServerData(withParamenters: [ApiKey.method : "artist.search", ApiKey.artist: seachText, ApiKey.limit: "20", ApiKey.page: "\(pageNumber)"], success: { results in
            guard let results = results, let artists = results.result?.artistmatches?["artist"] else { return }
            
            if self.pageNumber == 1 {
                self.artists = artists
            } else {
                self.artists.append(contentsOf: artists)
            }
            self.maxArtistPageNumber = Int(results.result?.totalResults ?? "1") ?? 1
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { error in
            DispatchQueue.main.async {
                self.alert(message: error)
            }
        }
    }
    
    private func getTracks(with seachText: String) {
        LastFMNetworkManager.getServerData(withParamenters: [ApiKey.method : "track.search", ApiKey.track: seachText, ApiKey.limit: "20", ApiKey.page: "\(pageNumber)"], success: { results in
            guard let results = results, let tracks = results.result?.trackmatches?["track"] else { return }
            
            if self.pageNumber == 1 {
                self.tracks = tracks
            } else {
                self.tracks.append(contentsOf: tracks)
            }
            self.maxTrackPageNumber = Int(results.result?.totalResults ?? "1") ?? 1
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { error in
            DispatchQueue.main.async {
                self.alert(message: error)
            }
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch categories[section] {
        case LabelText.songs:
            return tracks.count
        case LabelText.artists:
            return artists.count
        default:
            return albums.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeTableViewCell.self)) as? HomeTableViewCell else { return UITableViewCell() }
        
        switch categories[indexPath.section] {
        case LabelText.songs:
            let track = tracks[indexPath.row]
            cell.titleLabel.text = track.name
            cell.subtitleLabel.text = "Artist: \(track.artist)"
            cell.profileImageView.loadImageUsingCache(withUrl: track.image?.first?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        case LabelText.artists:
            let artist = artists[indexPath.row]
            cell.titleLabel.text = artist.name
            cell.subtitleLabel.text = "Listeners: \(artist.listeners)"
            cell.profileImageView.loadImageUsingCache(withUrl: artist.image?.first?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        default:
            let album = albums[indexPath.row]
            cell.titleLabel.text = album.name
            cell.subtitleLabel.text = "Artist: \(album.artist)"
            cell.profileImageView.loadImageUsingCache(withUrl: album.image?.first?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        }
        
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if categories.count == 1 {
            return 0
        }
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if categories.count == 1 {
            return nil
        }

        guard let sectionHeader =  Bundle.main.loadNibNamed(String(describing: HomeSectionHeader.self), owner: nil, options: nil)?.first as? HomeSectionHeader else { return nil }
        sectionHeader.titleLabel.text = categories[section]
        sectionHeader.seeAll = {
            let controller = HomeViewController.instantiate(fromAppStoryboard: .Main)
            switch self.categories[section] {
            case LabelText.songs:
                controller.categories = [LabelText.songs]
                controller.controllerType = ControllerType.songs
                controller.maxTrackPageNumber = self.maxTrackPageNumber
                controller.tracks = self.tracks
            case LabelText.artists:
                controller.categories = [LabelText.artists]
                controller.controllerType = ControllerType.artists
                controller.artists = self.artists
                controller.maxArtistPageNumber = self.maxArtistPageNumber
            default:
                controller.categories = [LabelText.albums]
                controller.controllerType = ControllerType.albums
                controller.albums = self.albums
                controller.maxAlbumPageNumber = self.maxAlbumPageNumber
            }
            controller.searchedText = self.searchedText
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if categories.count == 1 {
            switch categories[indexPath.section] {
            case LabelText.songs:
                if indexPath.row == tracks.count - 1 && pageNumber < maxTrackPageNumber {
                    pageNumber += 1
                    getTracks(with: searchedText)
                }
            case LabelText.artists:
                if indexPath.row == artists.count - 1 && pageNumber < maxArtistPageNumber {
                    pageNumber += 1
                    getArtists(with: searchedText)
                }
            default:
                if indexPath.row == albums.count - 1 && pageNumber < maxAlbumPageNumber {
                    pageNumber += 1
                    getAlbums(with: searchedText)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailViewController.instantiate(fromAppStoryboard: .Main)
        vc.focusedItemIndex  = indexPath.row
        switch categories[indexPath.section] {
        case LabelText.songs:
            vc.controllerType = .songs
            vc.array = tracks
        case LabelText.artists:
            vc.controllerType = .artists
            vc.array = artists
        case LabelText.albums:
            vc.controllerType = .albums
            vc.array = albums
        default:
            break
        }
        vc.modalPresentationStyle = .currentContext
        vc.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.async {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}

extension HomeViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .white
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        albums.removeAll()
        artists.removeAll()
        tracks.removeAll()
        searchedText = searchText
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if searchText.count > 0 {
            getTracks(with: searchText)
            getArtists(with: searchText)
            getAlbums(with: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        return
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
}
