//
//  HomeSectionHeader.swift
//  Last.FM
//
//  Created by Vasu Saini on 14/12/18.
//  Copyright © 2018 Vasu Saini. All rights reserved.
//

import UIKit

class HomeSectionHeader: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    
    var seeAll: (() -> Void)?

    @IBAction func seeAll(_ sender: Any) {
        seeAll?()
    }
}
