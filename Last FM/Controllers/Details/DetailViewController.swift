//
//  DetailViewController.swift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    /// label detail
    @IBOutlet weak var labelDetail: UILabel!
    
    /// button cross
    @IBOutlet weak var buttonCross: UIButton!
    
    /// collection view
    @IBOutlet weak var collectionViewDetail: UICollectionView! {
        didSet {
            collectionViewDetail.register(UINib(nibName: String(describing: DetailCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DetailCollectionViewCell.self))
        }
    }
    
    var array = [Any]()
    var controllerType = ControllerType.lastFM
    var focusedItemIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.collectionViewDetail.scrollToItem(at: IndexPath(item: self.focusedItemIndex, section: 0), at: .right, animated: false)
        }
    }

    //MARK: IBactions
    @IBAction func buttonCrossAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DetailCollectionViewCell.self), for: indexPath) as? DetailCollectionViewCell else { return UICollectionViewCell() }
        
        cell.labelThird.textColor = UIColor.blue
        var urlString = ""
        switch controllerType {
        case .albums:
            let album = array[indexPath.item] as! Album
            cell.confirgureCellForAlbum(with: album)
        case .artists:
            let artist = array[indexPath.item] as! Artist
            cell.confirgureCellForArtist(with: artist)
        case .songs:
            let track = array[indexPath.item] as! Track
            cell.confirgureCellForTrack(with: track)
        default:
            break
        }
        
        return cell
    }
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: self.view.frame.width , height: collectionViewDetail.frame.height)
        
        return size
    }
}
