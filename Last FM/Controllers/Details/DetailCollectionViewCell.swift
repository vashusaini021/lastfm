//
//  DetalCollectionViewswift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelSixth: UILabel!
    @IBOutlet weak var labelFifth: UILabel!
    @IBOutlet weak var labelFourth: UILabel!
    @IBOutlet weak var labelThird: UILabel!
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var labelFirst: UILabel!
    
    @IBOutlet weak var labelSixthHeading: UILabel!
    @IBOutlet weak var labelFifthHeading: UILabel!
    @IBOutlet weak var labelFourthHeading: UILabel!
    @IBOutlet weak var labelThirdHeading: UILabel!
    @IBOutlet weak var labelSecondHeading: UILabel!
    @IBOutlet weak var labelFirstHeading: UILabel!
    @IBOutlet weak var urlButton: UIButton!
    @IBOutlet weak var imageViewCell: UIImageView!
    
    var urlOpen: (() -> Void)?
    
    @IBAction func UrlButtonAction(_ sender: UIButton) {
        urlOpen?()
    }
    
    func confirgureCellForAlbum(with album: Album) {
        imageViewCell.loadImageUsingCache(withUrl: album.image?.last?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        labelFirstHeading.text = "Name:"
        labelSecondHeading.text = "Artist:"
        labelThirdHeading.text = "URL:"
        labelFourthHeading.text = "Streamale:"
        labelFifthHeading.text = "MBid:"
        
        labelFirst.text =  album.name
        labelSecond.text = album.artist
        labelThird.text =  album.url
        labelFourth.text = album.streamable
        labelFifth.text =  album.mbid
        urlOpen = {
            if let link = URL(string: album.url) {
                UIApplication.shared.open(link)
            }
        }
    }
    
    func confirgureCellForArtist(with artist: Artist) {
        imageViewCell.loadImageUsingCache(withUrl: artist.image?.last?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        labelFirstHeading.text = "Name:"
        labelSecondHeading.text = "Listeners:"
        labelThirdHeading.text = "URL:"
        labelFourthHeading.text = "Streamale:"
        labelFifthHeading.text = "MBid:"
        
        labelFirst.text =  artist.name
        labelSecond.text = artist.listeners
        labelThird.text =  artist.url
        labelFourth.text = artist.streamable
        labelFifth.text =  artist.mbid
        urlOpen = {
            if let link = URL(string: artist.url) {
                UIApplication.shared.open(link)
            }
        }
    }
    
    func confirgureCellForTrack(with track: Track) {
        imageViewCell.loadImageUsingCache(withUrl: track.image?.last?.text ?? "", placeHolder: #imageLiteral(resourceName: "noImageAvail"))
        labelFirstHeading.text = "Name:"
        labelSecondHeading.text = "Artist:"
        labelThirdHeading.text = "URL:"
        labelFourthHeading.text = "Streamale:"
        labelFifthHeading.text = "Listeners:"
        labelSixthHeading.text = "MBid:"
        
        labelFirst.text =  track.name
        labelSecond.text = track.artist
        labelThird.text =  track.url
        labelFourth.text = track.streamable
        labelFifth.text =  track.listeners
        labelSixth.text =  track.mbid
        urlOpen = {
            if let link = URL(string: track.url) {
                UIApplication.shared.open(link)
            }
        }
    }
}
