//
//  LastFMAPIManager.swift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

class LastFMNetworkManager {
    
    static func getServerData(withParamenters parameter: [String: String], success: @escaping(Results?) -> Void, failure: @escaping(String)-> Void) {
        var param = parameter
        param[LastFM.ApiKey.apiKey] = AppConfiguration.apiKey
        param[LastFM.ApiKey.format] = "json"
        var components = URLComponents()
        components.queryItems = param.map {
            URLQueryItem(name: $0, value: $1)
        }
        let urlString = AppConfiguration.baseUrl + (components.url?.absoluteString ?? "")
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error?.code == -1009 {
                failure(LastFM.AlertMessage.noInternet)
                
                return
            }
            guard error == nil && data != nil else { return }
            do {
                let results = try JSONDecoder().decode(Results.self, from: data!)
                success(results)
            } catch {
                failure(LastFM.AlertMessage.somethingWentWrong)
            }
            
            }.resume()
    }
}

extension Error {
    
    var code: Int { return (self as NSError).code }
}
