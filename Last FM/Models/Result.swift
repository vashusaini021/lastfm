//
//  Result.swift
//  Last FM
//
//  Created by Vashu on 18/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

struct Result: Codable {
    
    var attr: [String: String]?
    var albummatches: [String: [Album]]?
    var artistmatches: [String: [Artist]]?
    var trackmatches: [String: [Track]]?
    var openSearchQuery: [String: String]?
    var itemsPerPage: String
    var startIndex: String
    var totalResults: String
    
    
    enum CodingKeys: String, CodingKey {
        case attr = "@attr"
        case albummatches
        case artistmatches
        case trackmatches
        case openSearchQuery = "opensearch:Query"
        case itemsPerPage = "opensearch:itemsPerPage"
        case startIndex = "opensearch:startIndex"
        case totalResults = "opensearch:totalResults"
    }
}
