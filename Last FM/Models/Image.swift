//
//  Image.swift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

struct Image: Codable {
    
    var text: String
    var size: String
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }
}
