//
//  Results.swift
//  Last FM
//
//  Created by Vashu on 18/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

struct Results: Codable {
    
    var result: Result?
    
    enum CodingKeys: String, CodingKey {
        case result = "results"
    }
}
