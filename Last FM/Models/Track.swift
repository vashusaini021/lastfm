//
//  Track.swift
//  Last FM
//
//  Created by Vasu Saini on 15/12/18.
//  Copyright © 2018 Vashu. All rights reserved.
//

import Foundation

struct Track: Codable {
    
    var name: String
    var artist: String
    var mbid: String
    var url: String
    var streamable: String
    var listeners: String
    var image: [Image]?
}
